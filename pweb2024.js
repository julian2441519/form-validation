document.getElementById('loginForm').addEventListener('submit', function(event) {
    event.preventDefault(); 

    let username = document.getElementById('input_text').value;
    let password = document.getElementById('input_password').value;
    let nim = document.getElementById('input_nim').value;
    let prodi = document.getElementById('input_prodi').value;
    let errorMsg = document.getElementById('error_msg');

    if (username === "") {
        errorMsg.style.display = 'block';
        errorMsg.textContent = 'Username wajib diisi';
    } else if (password === "") {
        errorMsg.style.display = 'block';
        errorMsg.textContent = 'Password wajib diisi';
    } else if (nim === "") {
        errorMsg.style.display = 'block';
        errorMsg.textContent = 'nim wajib diisi';  
    }else if (prodi === "") {
        errorMsg.style.display = 'block';
        errorMsg.textContent = 'prodi wajib diisi';             
    } else {
        errorMsg.style.display = 'none';
        alert('Login sukses!');
    }
});
